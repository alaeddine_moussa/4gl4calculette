package org.esprit.calculator;

import javax.ejb.Stateless;

/**
 * @author alaeddine moussa
 * Session Bean implementation class Calculette
 */
@Stateless
public class Calculette implements CalculetteRemote, CalculetteLocal {

    /**
     * Default constructor. 
     */
    public Calculette() {
        // TODO Auto-generated constructor stub
    }

	@Override
	/**
	 * Cette methode permet d'additioner deux nombres
	 * @param a de type int
	 * 		  b de type int
	 * @return int
	 */
	public int add(int a, int b) {
		return a+b;		
	}

	@Override
	/**
	 * Cette methode permet de soustraire deux nombres
	 * @param a de type int
	 * 		  b de type int
	 * @return int
	 */
	public int subtract(int a, int b) {
		return a-b;
	}

	
}
