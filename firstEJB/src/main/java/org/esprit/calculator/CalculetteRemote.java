package org.esprit.calculator;

import javax.ejb.Remote;

@Remote
public interface CalculetteRemote {
	public int add(int a, int b);
	public int subtract(int a, int b); 
}
